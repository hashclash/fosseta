import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule }   from '@angular/router';
import {MaterialModule} from './material.module'
import { AppComponent } from './app.component';

import{ HomeComponent} from './home.component'
import {BrandComponent} from './brand.component'
import {CollectionComponent} from './collection.component'
import {ContactComponent} from './contact.component'
import {PhotoshootComponent} from './photoshoot.component'
import {InviteComponent} from './invite.component'
import {LoginComponent} from './login.component'


@NgModule({
  declarations: [
    AppComponent,HomeComponent,BrandComponent,CollectionComponent,
    PhotoshootComponent,InviteComponent,ContactComponent,LoginComponent
  ],
  imports: [
    BrowserModule,MaterialModule,BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path:'designer',
        component: HomeComponent
      },
      {
      path:'brand',
      component: BrandComponent
    },
    {
      path:'collection',
      component: CollectionComponent
    },
    {
      path:'photoshoot',
      component: PhotoshootComponent
    },   
    {
      path:'invite',
      component: InviteComponent
    },
    {
      path:'contact',
      component: ContactComponent
    },
    {
      path:'login',
      component: LoginComponent
    }

    
    ])
  
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

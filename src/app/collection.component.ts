import {Component} from '@angular/core'

@Component({
    selector:'collection',
    templateUrl:'./views/collection.html',
    styleUrls:['./styles/style.css']
})

export class CollectionComponent {
   
  
        tiles = [
            {text: 'One', cols: 3, rows: 4, color: 'lightblue',image:"../assets/image/soch.jpg",show:true},
            {text: 'Two', cols: 2, rows: 2, color: 'lightgreen'},
            {text: '*Inspiration*', cols: 2, rows: 1, color: '#00BCD4'},
            {text: '*Philosophy*', cols: 2, rows: 1, color: '#FF9800'},

            {text: 'five', cols: 1, rows: 4, color: 'lightblue',image:"../assets/image/arya.jpg",show:true},
            {text: 'six', cols: 1, rows: 4, color: 'lightgreen',image:"../assets/image/arya.jpg",show:true},
            {text: 'seven', cols: 1, rows: 4, color: 'lightpink',image:"../assets/image/arya.jpg",show:true},
            {text: 'Meet-  Take an Appointment', cols: 2, rows: 2, color: '#DDBDF1'},
            {text: 'nine', cols: 2, rows: 2, color: '#DDBDF1'},
            
          ];
    
}
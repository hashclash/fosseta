import {Component} from '@angular/core'

@Component({
    selector:'home',
    templateUrl:'/views/home.html',
    styleUrls:['/styles/style.css']
})


export class HomeComponent{
    title:string;
    subtitle:string;
    ImagePath: string;
    constructor(){
        this.title ="Fosseta Moda"
        this.subtitle="by Dimple Arora"
        this.ImagePath ="../assets/image/woman-placeholder.jpg"
    }
}


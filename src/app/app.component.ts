import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './views/app.component.html',
  styleUrls:['./styles/style.css']
})
export class AppComponent {
  
  title = 'app';
  navList: string[];
  constructor(){
    this.navList=["The Brand" ,"The Designer"," Collection", "Photoshoots","Contact","Invite A friend"];
  }
}

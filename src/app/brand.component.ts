import {Component} from '@angular/core'
@Component({
    selector:"brand",
    templateUrl:"./views/brand.html",
    styleUrls:['./styles/style.css']
})

export class BrandComponent{
brandDescription:String;
constructor(){
    this.brandDescription ='As a brand , Fossetta Moda is a new clothing online store , it specializes in designer women apparel with contemporary silhouettes and details. It is coming in this world of fashion with a spirit of progressivism and distinctiveness in its art of designing. It believes that the clothes you wear speak a lot about who you are; hence Fossetta Moda aims at giving you a perfect look with its designs.'+
    '“Your Apparel will always speak all about your personality; as how you look is the first impression of yours on anyone.”'+
    'But,'+
    'It doesn’t mean that “more expensive the outfit, the better a person looks." It actually means than a person can look perfect in voguish yet affordable outfits.'+
    'Fossetta Moda is a brand for strong and bold men and women who represent a mentality of fighting for what they believe in through the art of cloth by breaking the myraid sterotyping notion of traditional fashion.'+
     '     What does it want to achieve?'+
    'For the brand to become the predilection among all the class of people living in the society, from poor to rich ; everyone must love to wear the simple yet sleeky, casual yet occasional, elegant yet appropriate designs from Fossetta Moda.'
}

}
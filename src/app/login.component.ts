import {Component} from '@angular/core'
import {FormControl, Validators} from '@angular/forms';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
    selector:'login',
    templateUrl:'./views/login.html',
    styleUrls:['./styles/style.css']
})
export class LoginComponent {
    
    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.pattern(EMAIL_REGEX)]);
    
    constructor(){

    }
}